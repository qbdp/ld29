(define *batches* (make-hash-table))

(define-record-type :batch
                    (make-batch data texture quads)
                    batch?
                    (data batch-data batch-data-set!)
                    (texture batch-texture)
                    (quads batch-quads batch-quads-set!))

; TODO: use triangles
(define (batch-quad! a b c d u v t)
  (unless (hash-table-exists? *batches* (texture-id t))
    ;(print "new batch: " (texture-id t))
    (hash-table-set! *batches* (texture-id t) (make-batch '() t 0)))
  (let ((batch (hash-table-ref *batches* (texture-id t))))
    (batch-data-set! batch (append (list (v2-x a) (v2-y a) (v2-x u) (v2-y u)
                                         (v2-x b) (v2-y b) (v2-x v) (v2-y u)
                                         (v2-x c) (v2-y c) (v2-x v) (v2-y v)
                                         (v2-x d) (v2-y d) (v2-x u) (v2-y v))
                                   (batch-data batch)))
    (batch-quads-set! batch (+ 1 (batch-quads batch)))))
