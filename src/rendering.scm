(define *window* #f)
(define *window-width* 640)
(define *window-height* 480)
(define *atlas-page-size* (v2 1024 1024))
(define *zoom-level* 3)
(define *texture-cache* (make-hash-table))
(define *current-atlas* #f)
(define *texture* 0)
(define *camera* 0)

(define-record-type :texture
  (make-texture id size offset)
  texture?
  (id texture-id)
  (size texture-size)
  (offset texture-offset))

(define (texture-width texture)
  (v2-x (texture-size texture)))

(define (texture-height texture)
  (v2-y (texture-size texture)))

(define (texture-offset-uv texture)
  (v2-divv (texture-offset texture) *atlas-page-size*))

; initialize opengl renderer
(define (init-renderer)
  (glfwInit)
  (set! *window* (glfwCreateWindow *window-width* *window-height* "Ein Boot" #f #f))
  (glfwMakeContextCurrent *window*)
  (glfwSwapInterval 0)
  ; init opengl
  (gl:ClearColor .5 .5 .5 1)
  (gl:Viewport 0 0 *window-width* *window-height*)
  (gl:Ortho 0 (/ *window-width* *zoom-level*) (/ *window-height* *zoom-level*) 0 0 100)
  (gl:Enable gl:TEXTURE_2D)
  (gl:Enable gl:BLEND)
  (gl:BlendFunc gl:SRC_ALPHA gl:ONE_MINUS_SRC_ALPHA))

(define (begin-render)
  (hash-table-clear! *batches*)
  (gl:Clear gl:COLOR_BUFFER_BIT))

(define (end-render)
  (gl:EnableClientState gl:VERTEX_ARRAY)
  (gl:EnableClientState gl:TEXTURE_COORD_ARRAY)
  ;(print "rendering " (hash-table-size *batches*) " batches")
  (hash-table-for-each *batches* (lambda (key value) (render-batch value)))
  (gl:DisableClientState gl:VERTEX_ARRAY)
  ;(gl:DisableClientState gl:TEXTURE_COORD_ARRAY)
  (glfwSwapBuffers *window*))

(define (render-quad a b c d u v)
  ;(batch-quad! a b c d u v *texture*))
  (gl:Begin gl:QUADS)
  (gl:TexCoord2d (v2-x u) (v2-y u))
  (gl:Vertex2f (v2-x a) (- (v2-y a) *camera*))
  (gl:TexCoord2d (v2-x v) (v2-y u))
  (gl:Vertex2f (v2-x b) (- (v2-y b) *camera*))
  (gl:TexCoord2d (v2-x v) (v2-y v))
  (gl:Vertex2f (v2-x c) (- (v2-y c) *camera*))
  (gl:TexCoord2d (v2-x u) (v2-y v))
  (gl:Vertex2f (v2-x d) (- (v2-y d) *camera*))
  (gl:End))

(define (render-aabb a b u v)
  (batch-quad! a 
               (v2 (v2-x b) (v2-y a))
               b
               (v2 (v2-x a) (v2-y b))
               u v *texture*))

(define (render-batch batch)
  (let* ((data (list->f32vector (batch-data batch)))
         (array (make-locative data)))
    (gl:BindTexture gl:TEXTURE_2D (texture-id (batch-texture batch)))
    (gl:VertexPointer 2 gl:FLOAT 16 array)
    (gl:TexCoordPointer 2 gl:FLOAT 16 (pointer+ array 8))
    (gl:DrawArrays gl:QUADS 0 (* 4 (batch-quads batch)))))

(define (render-actor actor)
  (process-message! actor '(render))
  (when (actor-sprite actor)
    (sprite-update! (actor-sprite actor) *dt*)
    (render-sprite (actor-position actor) (actor-rotation actor)
                   (actor-sprite actor))))

(define (render-sprite position rotation sprite)
  (set-texture (sprite-texture sprite))
  ;(sprite-frame-set! sprite 6)
  (match-let (((u v) (sprite-frame-uv sprite)))
    ; FIXME: hardware rotation
    (render-quad position
                 (v2-add position (v2-rotate (v2 (sprite-width sprite) 0) rotation))
                 (v2-add position (v2-rotate (sprite-size sprite) rotation))
                 (v2-add position (v2-rotate (v2 0 (sprite-height sprite)) rotation)) u v)))

(define (render-text position font-name text)
  (set-texture (font-texture (load-font font-name)))
  ;(render-quad (v2 0 0) (v2 256 256) (v2 0 0) (v2 1 1))
  (let loop ((font (load-font font-name))
             (chars (string->list text))
             (pen position))
    (let ((metrics (font-char-metrics font (car chars))))
      (let ((pen (v2-add pen (list-ref metrics 4)))
            (offset (texture-offset-uv (font-texture font))))
        (render-aabb pen (v2-add pen (first metrics))
                     (v2-add offset (v2-divv (second metrics) *atlas-page-size*))
                     (v2-add offset (v2-divv (third metrics) *atlas-page-size*))))
      (unless (null? (cdr chars))
        (loop font (cdr chars) (v2-add pen (cadddr metrics)))))))

(define (set-texture texture)
  (set! *texture* texture))

(define (upload-image image)
  (let* ((id-vector (make-u32vector 1))
         (error (gl:GenTextures 1 id-vector))
         (id (u32vector-ref id-vector 0)))
    (print* "uploading " (/ (* 4 (image-width image) (image-height image)) 1024)
           " kb to the gpu...")
    (gl:BindTexture gl:TEXTURE_2D id)
    (gl:TexParameteri gl:TEXTURE_2D gl:TEXTURE_WRAP_S gl:CLAMP_TO_EDGE)
    (gl:TexParameteri gl:TEXTURE_2D gl:TEXTURE_WRAP_T gl:CLAMP_TO_EDGE)
    (gl:TexParameterf gl:TEXTURE_2D gl:TEXTURE_MIN_FILTER gl:NEAREST)
    (gl:TexParameterf gl:TEXTURE_2D gl:TEXTURE_MAG_FILTER gl:NEAREST)
    (gl:TexImage2D gl:TEXTURE_2D 0 gl:RGBA (image-width image) (image-height image) 0
                   gl:RGBA gl:UNSIGNED_BYTE
                   (make-locative (image-pixels image)))
    (print " texture uploaded")
    (make-texture id (image-size image) (v2 0 0))))

; tries to fit an image into the current atlas and generates a new one if it runs out of space
(define (atlas-fit! image #!optional (position (v2 0 0)))
  (unless *current-atlas* (reset-atlas))
  (let* ((atlas (car *current-atlas*))
        (textures (cadr *current-atlas*))
        (overlaps (find (lambda (t)
                          (overlaps? position (image-size image)
                                     (texture-offset t) (texture-size t)))
                        textures)))
    (cond (overlaps
            (atlas-fit! image (v2-add position (v2 (v2-x (texture-size overlaps)) 0))))
          ((> (+ (v2-x position) (image-width image)) (v2-x *atlas-page-size*))
            ; FIXME: skip minimum possible height
            (atlas-fit! image (v2 0 (+ 1 (v2-y position)))))
          ((not (contains? (v2-add position (image-size image)) (v2 0 0) *atlas-page-size*))
           (reset-atlas)
           (atlas-fit! image))
          (else
            (print "fit image into the current atlas at " position)
            (gl:BindTexture gl:TEXTURE_2D (texture-id atlas))
            (gl:TexSubImage2D gl:TEXTURE_2D 0 (v2-x position) (v2-y position)
                              (image-width image) (image-height image) gl:RGBA
                              gl:UNSIGNED_BYTE (make-locative (u8vector->blob/shared
                                                           (image-pixels image))))
            (let ((texture (make-texture (texture-id atlas) (image-size image) position)))
              (set! *current-atlas* `(,(car *current-atlas*)
                                       ,(cons texture (cadr *current-atlas*))))
              texture)))))

; generates or updates the atlas with the latest image data
(define (reset-atlas)
  (print "generating new atlas")
  (set! *current-atlas* (list (upload-image (make-empty-image *atlas-page-size*)) '())))

(define (load-texture name)
  (if (hash-table-exists? *texture-cache* name)
    (hash-table-ref *texture-cache* name)
    (let ((image (load-image (conc "data/images/" name))))
      (hash-table-set! *texture-cache* name (atlas-fit! image))
      (load-texture name))))
