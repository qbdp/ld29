(define *actor-definitions* '())
(define *current-actor* #f)

(define-record-type :actor
  (build-actor position rotation velocity layer sprite behaviors data)
  actor?
  (position actor-get-position actor-position-set!)
  (rotation actor-get-rotation actor-rotation-set!) 
  (velocity actor-get-velocity actor-velocity-set!)
  (layer actor-get-layer actor-layer-set!)
  (sprite actor-get-sprite actor-sprite-set!)
  (behaviors actor-behaviors actor-behaviors-set!)
  (data actor-data actor-data-set!))

(define (make-actor position layer sprite behaviors data)
  (build-actor position 0 (v2 0 0) layer sprite behaviors data))

(define-syntax get-implicit-actor
  (er-macro-transformer
    (lambda (form rename compare?)
        '(if (null? actor) *current-actor* (car actor)))))

; TODO: use a macro for this!

(define (actor-position . actor)
  (actor-get-position (get-implicit-actor)))

(define (actor-rotation . actor)
  (actor-get-rotation (get-implicit-actor)))

(define (actor-velocity . actor)
  (actor-get-velocity (get-implicit-actor)))

(define (actor-layer . actor)
  (actor-get-layer (get-implicit-actor)))

(define (actor-sprite . actor)
  (actor-get-sprite (get-implicit-actor)))

(define (actor-x . actor)
  (v2-x (actor-position (get-implicit-actor))))

(define (actor-y . actor)
  (v2-y (actor-position (get-implicit-actor))))

; TODO: macros for implicit quoting
(define ($ key . actor)
  (cdr (assoc key (actor-data (get-implicit-actor)))))

(define ($! key value . actor)
  (actor-data-set! (get-implicit-actor)
                   (alist-update! key value (actor-data (get-implicit-actor)))))

(define-syntax define-actor
  (er-macro-transformer
    (lambda (form rename compare?)
      (let* ((name (cadr form))
             (sprite (caddr form))
             (behaviors (cons 'default-behavior (cadddr form)))
             (embedded-behavior-name (symbol-append 'embedded-behavior- name)))
        `(begin
           ,(if (> (length form) 4)
              `(define-behavior ,embedded-behavior-name ,@(cddddr form))
              #f)
           (set! *actor-definitions*
             (cons (list ',name (list ',sprite
                                      ',(if (> (length form) 4)
                                          (cons embedded-behavior-name behaviors)
                                          behaviors)))
                   *actor-definitions*)))))))

(define (append-behavior! actor behavior)
  (actor-behaviours-set! (cons behavior (actor-behaviors actor))))

(define (update-actor! actor dt)
  (process-message! actor `(move ,(v2-scale (actor-velocity actor) dt)))
  (process-message! actor `(update ,dt)))

(define (process-global-message! message)
  (for-each (lambda (actor) (process-message! actor message))
            (scene-actors (car *scene-stack*))))

(define (process-message! actor message)
  (let loop ((behaviors (actor-behaviors actor)))
    (unless (null? behaviors)
      (set! *current-actor* actor)
      ((behavior-procedure (car behaviors)) actor message)
      (loop (cdr behaviors))))
  ; handle system messages
  (case (car message)
    ((move)
     (actor-position-set! actor (v2-add (cadr message) (actor-position actor))))
    ((velocity)
     (actor-velocity-set! actor (cadr message)))
    ((rotation)
     (actor-rotation-set! actor (cadr message)))))
