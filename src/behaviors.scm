(define-behavior default-behavior
  (((accelerate) v)
   (msg velocity ,(v2-add (actor-velocity) v)))
  (((rotate) a)
   (msg rotation ,(+ (actor-rotation) a))))

(define-behavior move-left
  (((update) dt)
   ;(msg move ,(v2-scale (v2 -100 (* (sin (/ (actor-x) 50)) 200)) dt))
   (msg rotate ,(* 1 dt)))
  (((render))
   #;(render-text (actor-position) "proggy-clean.ttf" "hi, I'm a moon")))
