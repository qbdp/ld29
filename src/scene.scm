(define-record-type :scene
  (build-scene actors)
  scene?
  (actors scene-actors scene-actors-set!))

(define (make-scene)
  (build-scene '()))

(define *scene-stack* '())

(define (push-scene scene)
  (set! *scene-stack* (cons scene *scene-stack*)))

(define (pop-scene)
  (set! *scene-stack* (cdr *scene-stack*)))

(define (append-actor! scene actor)
  ; TODO: insertion sort
  (scene-actors-set! scene (sort! (cons actor (scene-actors scene))
                                  (lambda
                                    (a b)
                                    (< (actor-layer a) (actor-layer b)))))
  (process-message! actor '(init)))

(define (load-scene name)
  (print "loading scene " name)
  (with-input-from-file
    (conc "data/scenes/" name)
    (lambda ()
      (let loop ((sexp (read)) (scene (make-scene)))
        (unless (eof-object? sexp)
          (case (car sexp)
            ((actor)
             (print "new " (cadr sexp) " actor")
             (let* ((position (v2 (list-ref sexp 2) (list-ref sexp 3)))
                    (actor-definition (cadr (assoc (cadr sexp) *actor-definitions*)))
                    (sprite (if (car actor-definition)
                              (load-sprite (car actor-definition))
                              #f))
                    (behaviors (map make-behavior (eval (cons list (cadr actor-definition)))))
                    (layer (list-ref sexp 4))
                    (data (list-ref sexp 5)))
               (append-actor! scene (make-actor position layer sprite behaviors data))))
            (else (print "unknown datatype '" (car sexp) "'")))
          (loop (read) scene))
        scene))))
