(foreign-declare "#include <AS3/AS3.h>")

(print "installing flash extensions...")

; entry point
(define-external (update_external) void (step))

(define (step)
  (update 0.01)
  (render))

(define (main)
  (define go-async (foreign-safe-lambda* void () "AS3_GoAsync();"))
  (print "entering async mode...")
  (go-async)
  (quit))

; stage3d renderer
(define (init-renderer)
    (define init-renderer-as3
      (foreign-lambda*
        void ((int width) (int height))
        "inline_as3(
        \"import com.adobe.flascc.CModule;\"
        \"CModule.activeConsole.initRenderer(%0, %1);\"
        : : \"r\"(width), \"r\"(height));"))
      (init-renderer-as3 *window-width* *window-height*))

(define begin-render
    (foreign-lambda*
      void ()
      "inline_as3(
      \"import com.adobe.flascc.CModule;\"
      \"CModule.activeConsole.beginRender();\"
      : : );"))

(define end-render
    (foreign-lambda*
      void ()
      "inline_as3(
      \"import com.adobe.flascc.CModule;\"
      \"CModule.activeConsole.endRender();\"
      : : );"))

(define draw-triangle
    (foreign-lambda*
      void ()
      "inline_as3(
      \"import com.adobe.flascc.CModule;\"
      \"CModule.activeConsole.drawTriangle();\"
      : : );"))

(define (set-texture texture)
  (print "texture ''set''"))

(define (load-texture name)
  (make-texture 0 (v2 32 32) (v2 0 0)))

; input
(define (update-input)
  (print "updating input"))
