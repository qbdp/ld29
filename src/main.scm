(use chicken-syntax srfi-1 srfi-4 srfi-69 lolevel data-structures)

(define *dt* 0.01)

;#+(not emscripten flash) (use tcp)
#+(not standalone) (use matchable gl glfw3 freetype stb_image)
#+standalone (include "src/platform-standalone.scm")

(include "src/math.scm")
(include "src/image.scm")
(include "src/sprite.scm")
(include "src/batching.scm")
(include "src/rendering.scm")
(include "src/input.scm")
(include "src/debug.scm")
(include "src/scene.scm")
(include "src/behavior.scm")
(include "src/behaviors.scm")
(include "src/actor.scm")
(include "src/actors.scm")
(include "src/text.scm")

(define (main)
  ;(define netrepl (make-netrepl))
  ; TODO: restore livecoding continuation system
  (let loop ((previous-time 0)
             (current-time (clock)))
      ;(netrepl)
      (update (- current-time previous-time))
      (render)
  (loop current-time (clock))))

#+flash (include "src/platform-flash.scm")

(init-renderer)
(init-input)

(push-scene (load-scene "game.scm"))

(define (update dt)
  (set! *dt* dt)
  (update-input)
  (for-each (lambda (actor) (update-actor! actor dt)) (scene-actors (car *scene-stack*))))

(define (render)
  (begin-render)
  ;(draw-triangle)
  (for-each render-actor (scene-actors (car *scene-stack*)))
  (render-text (v2 10 200) "proggy-clean.ttf" "Render text test")
  (end-render))

(main)
