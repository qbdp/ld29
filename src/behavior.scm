(define-record-type :behavior
  (build-behavior procedure data)
  behavior?
  (procedure behavior-procedure)
  (data behavior-data behavior-data-set!))

(define (make-behavior procedure)
  (build-behavior procedure '()))

(define-syntax msg
  (er-macro-transformer
    (lambda (form rename compare?)
      `(process-message! actor ,(list 'quasiquote (cdr form))))))

(define-syntax define-behavior
  (er-macro-transformer
    (lambda (form rename compare?)
      (let ((name (cadr form))
            (conditions (cddr form)))
        `(define (,name actor message)
           (match message
                  ,@(map (lambda (segment) `(((or ,@(map (lambda (item) `(quote ,item))
                                                         (caar segment)))
                                              ,@(cdar segment) . rest)
                                             ,@(cdr segment))) conditions)
                  (else)))))))
