(define-actor background background ())

(define-actor system #f ()
  (((press) 'quit)
   (print "closing the game by user request...")
   (quit)))

(define-actor player captain ()
  (((init))
    (print "initialized player")
    (sprite-play! (actor-sprite) 'walk))
  (((update))
   (set! *camera* (+ -60 (v2-y (actor-position)))))
  (((axis) 'vertical value)
   (msg move ,(v2 0 (* 32 value)))))

(define-actor water water ()
  (((init))
   ($! 'offset (v2 0 0)))
  (((update) dt)
   ($! 'offset (v2-add (v2 0 (* 9 dt)) ($ 'offset)))
   (if (> (v2-y ($ 'offset)) 32) ($! 'offset (v2 0 0))))
  (((render))
   (do ((y -32 (+ y 32)))
     ((> y 520))
     (do ((x 0 (+ x 32)))
       ((> x 196))
       (render-sprite (v2-add ($ 'offset) (v2 x y)) 0 (actor-sprite))))))
