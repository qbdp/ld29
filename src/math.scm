; 2D vector math
(define-record-type :v2
  (v2 x y)
  v2?
  (x v2-x)
  (y v2-y))

(define-record-printer (:v2 v out)
  (fprintf out "#,(v2 ~S ~S)" (v2-x v) (v2-y v)))

(define (v2-add a b)
  (v2 (+ (v2-x a) (v2-x b)) (+ (v2-y a) (v2-y b))))

(define (v2-sub a b)
  (v2 (- (v2-x a) (v2-x b)) (- (v2-y a) (v2-y b))))

(define (v2-scale v s)
  (v2 (* (v2-x v) s) (* (v2-y v) s)))

(define (v2-div v s)
  (v2 (/ (v2-x v) s) (/ (v2-y v) s)))

(define (v2-divv a b)
  (v2 (/ (v2-x a) (v2-x b)) (/ (v2-y a) (v2-y b))))

(define (v2-wrap-x v x y)
  (v2 (modulo (v2-x v) x) (+ (v2-y v) (* (// (v2-x v) x) y))))

(define (v2-wrap-y v y x)
  (v2 (+ (v2-x v) (* (// (v2-y v) y) x)) (modulo (v2-y v) y)))

(define (v2-rotate v a)
  (let ((cosine (cos a))
        (sine (sin a)))
    (v2 (- (* (v2-x v) cosine) (* (v2-y v) sine))
        (+ (* (v2-x v) sine) (* (v2-y v) cosine)))))

; rect math
(define (contains? point position size)
  (and (>= (v2-x point) (v2-x position)) (<= (v2-x point) (+ (v2-x position) (v2-x size)))
       (>= (v2-y point) (v2-y position)) (<= (v2-y point) (+ (v2-y position) (v2-y size)))))

(define (overlaps? positiona sizea positionb sizeb)
  (and (<= (v2-x positiona) (+ (v2-x positionb) (v2-x sizeb)))
       (>= (+ (v2-x positiona) (v2-x sizea)) (v2-x positionb))
       (<= (v2-y positiona) (+ (v2-y positionb) (v2-y sizeb)))
       (>= (+ (v2-y positiona) (v2-y sizea)) (v2-y positionb))))

; convenience functions
; integer division
(define (// a b)
  (floor (/ a b)))
