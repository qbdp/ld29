(define-record-type :sprite
  (build-sprite texture size frame animations timer aframe animation)
  sprite?
  (texture sprite-texture sprite-texture-set!)
  (size sprite-size sprite-size-set!)
  (frame sprite-frame sprite-frame-set!)
  (animations sprite-animations sprite-animations-set!)
  (timer sprite-timer sprite-timer-set!)
  (aframe sprite-aframe sprite-aframe-set!)
  (animation sprite-animation sprite-animation-set!))

(define (make-sprite texture size animations)
  (build-sprite texture size 0 animations 0 0 #f))

(define (sprite-width sprite)
  (v2-x (sprite-size sprite)))

(define (sprite-height sprite)
  (v2-y (sprite-size sprite)))

(define (sprite-play! sprite animation)
  (sprite-animation-set! sprite (cdr (assoc animation (sprite-animations sprite))))
  (sprite-frame-set! sprite (car (sprite-animation sprite)))
  (sprite-aframe-set! sprite 0)
  (sprite-timer-set! sprite (caadr (sprite-animation sprite))))

(define (sprite-update! sprite dt)
  (when (sprite-animation sprite)
    (sprite-timer-set! sprite (- (sprite-timer sprite) (* dt 1000)))
    (when (< (sprite-timer sprite) 0)
      (sprite-aframe-set! sprite (modulo (+ 1 (sprite-aframe sprite))
                                         (length (sprite-animation sprite))))
      (let ((frame (list-ref (sprite-animation sprite) (sprite-aframe sprite))))
        (sprite-frame-set! sprite (car frame))
        (sprite-timer-set! sprite (cadr frame))))))

(define (sprite-frame-uv sprite)
  (let* ((width (v2-x *atlas-page-size*))
         (height (v2-y *atlas-page-size*))
         (column (/ (modulo (* (sprite-width sprite) (sprite-frame sprite)) width)
                    (sprite-width sprite)))
         (row (floor (/ (sprite-frame sprite) (/ width (sprite-width sprite)))))
         (offset (texture-offset-uv (sprite-texture sprite))))
    (list
      (v2-add offset
        (v2
          (/ (* column (sprite-width sprite)) width)
          (/ (* row (sprite-height sprite)) height)))
      (v2-add offset
              (v2
                (/ (+ (* column (sprite-width sprite)) (sprite-width sprite)) width)
                (/ (+ (* row (sprite-height sprite)) (sprite-height sprite)) height))))))

; TODO: cache this
(define (load-sprite name)
  (with-input-from-file
    "data/sprites.scm"
    (lambda ()
      (let loop ((sexp (read)))
        (if (eqv? (cadr sexp) name)
          (begin
            (case (car sexp)
              ((sprite)
               (print "loaded sprite '" (cadr sexp) "'")
               (make-sprite
                 (load-texture (caddr sexp))
                 (v2 (list-ref sexp 3) (list-ref sexp 4))
                 (if (< (length sexp) 6)
                   #f
                   (list-ref sexp 5))))))
          (loop (read)))))))
