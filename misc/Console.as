/* Copyright (c) 2013 Adobe Systems Inc

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.adobe.flascc {
    import flash.display.DisplayObjectContainer;
    import flash.display.Sprite;
    import flash.display.StageScaleMode;
    import flash.events.Event;
    import flash.net.LocalConnection;
    import flash.net.URLRequest;
    import flash.text.TextField;
    import flash.text.TextFormat;
    import flash.utils.ByteArray;

    import flash.geom.*;
    import flash.display3D.*;
    import com.adobe.utils.AGALMiniAssembler;
    import flash.utils.getTimer;

    import com.adobe.flascc.vfs.ISpecialFile;
    import com.adobe.flascc.vfs.LSOBackingStore;
    import com.adobe.flascc.vfs.xvfs;

    public class Console extends Sprite implements ISpecialFile {
        private var enableConsole:Boolean = true;
        private var tf:TextField;
        private var inputContainer:DisplayObjectContainer;

        public var windowWidth:int;
        public var windowHeight:int;
        public var context3D:Context3D;
        public var program:Program3D;
        public var vertexbuffer:VertexBuffer3D;
        public var indexbuffer:IndexBuffer3D;

        private var alclogo:String = "FlasCC";

        /**
         * To Support the preloader case you might want to have the Console
         * act as a child of some other DisplayObjectContainer.
         */
        public function Console(container:DisplayObjectContainer = null) {
            CModule.rootSprite = container ? container.root : this;

            if(container) {
                container.addChild(this);
                init(null);
            } else {
                addEventListener(Event.ADDED_TO_STAGE, init);
            }
        }

        protected function init(e:Event):void {
            inputContainer = new Sprite();
            addChild(inputContainer);
            stage.frameRate = 60;
            stage.scaleMode = StageScaleMode.NO_SCALE;
            if(enableConsole) {
                tf = new TextField();
                tf.multiline = true;
                tf.width = stage.stageWidth;
                tf.height = stage.stageHeight;
                tf.background = false;
                var format:TextFormat = new TextFormat();
                format.font = "Courier New";
                tf.defaultTextFormat = format;
                inputContainer.addChild(tf);
            }
            CModule.vfs.console = this;
            CModule.vfs.addBackingStore(new xvfs(), null);
            CModule.vfs.addDirectory("/local");
            CModule.vfs.addBackingStore(new LSOBackingStore("samplevfsLSO"), "/local");
            var ba:ByteArray = new ByteArray();
            ba.writeUTFBytes(alclogo);
            CModule.vfs.addFile("/flascclogo.txt", ba);
            stage.stage3Ds[0].addEventListener(Event.CONTEXT3D_CREATE, init3D);
            stage.stage3Ds[0].requestContext3D(); 
        }

        protected function init3D(e:Event):void {
            context3D = stage.stage3Ds[0].context3D;
            context3D.configureBackBuffer(800, 600, 2, true);
            var vertices:Vector.<Number> = Vector.<Number>(
                    [0, 0, 0,
                    1, 0, 0,
                    100, 0, 0,
                    0, 1, 0,
                    100, 30, 0,
                    0, 0, 1]);
            vertexbuffer = context3D.createVertexBuffer(3, 6);
            // Upload VertexBuffer3D to GPU. Offset 0, 3 vertices
            vertexbuffer.uploadFromVector(vertices, 0, 3);
            var indices:Vector.<uint> = Vector.<uint>([0, 1, 2]);
            // Create IndexBuffer3D. Total of 3 indices. 1 triangle of 3 vertices
            indexbuffer = context3D.createIndexBuffer(3);
            // Upload IndexBuffer3D to GPU. Offset 0, count 3
            indexbuffer.uploadFromVector (indices, 0, 3);
            var vertexShaderAssembler : AGALMiniAssembler = new AGALMiniAssembler();
            vertexShaderAssembler.assemble(Context3DProgramType.VERTEX,
                    "m44 op, va0, vc0\n"  +
                    "mov v0, va1");
            var fragmentShaderAssembler : AGALMiniAssembler= new AGALMiniAssembler();
            fragmentShaderAssembler.assemble( Context3DProgramType.FRAGMENT,
                    "mov oc, v0" );
            program = context3D.createProgram();
            program.upload(vertexShaderAssembler.agalcode, fragmentShaderAssembler.agalcode);
            addEventListener(Event.ENTER_FRAME, enterFrame);
            consoleWrite("[as3] initialized 3D context\n");
            // start the game
            var args:Vector.<String> = new Vector.<String>();
            args.push("rockets");
            var environment:Vector.<String> = new Vector.<String>();
            CModule.startAsync(this);
            //CModule.start(this, args, environment, true);
            //CModule.startBackground(this, new <String>[], new <String>[])
        }

        public function exit(code:int):Boolean {
            return false;
        }

        public function write(fd:int, bufPtr:int, nbyte:int, errnoPtr:int):int {
            var str:String = CModule.readString(bufPtr, nbyte);
            consoleWrite(str);
            return nbyte;
        }

        public function read(fd:int, bufPtr:int, nbyte:int, errnoPtr:int):int {
            return 0;
        }

        public function fcntl(fd:int, com:int, data:int, errnoPtr:int):int {
            return 0;
        }

        public function ioctl(fd:int, com:int, data:int, errnoPtr:int):int {
            return 0;
        }

        public function consoleWrite(s:String):void {
            trace(s);
            if(enableConsole) {
                tf.appendText(s);
                tf.scrollV = tf.maxScrollV;
            }
        }

        public function initRenderer(width:int, height:int) {
            windowWidth = width;
            windowHeight = height;
            consoleWrite("[as3] width: " + width + " height: " + height + "\n");
        }

        public function beginRender():void {
            context3D.clear(1, 0, 1, 1);
            context3D.setVertexBufferAt(0, vertexbuffer, 0, Context3DVertexBufferFormat.FLOAT_3);
            context3D.setVertexBufferAt(1, vertexbuffer, 3, Context3DVertexBufferFormat.FLOAT_3);
            context3D.setProgram(program);
            context3D.setProgramConstantsFromMatrix(Context3DProgramType.VERTEX,
                    0, createOrthographicProjection(windowWidth, windowHeight,
                    -1, 100), true);
        }

        public function endRender():void {
            context3D.present();
        }

        public function drawTriangle():void {
            /*var m:Matrix3D = new Matrix3D();
            m.appendRotation(getTimer() / 40, Vector3D.Z_AXIS);
            context3D.setProgramConstantsFromMatrix(Context3DProgramType.VERTEX,
            0, m, true);*/
            context3D.drawTriangles(indexbuffer);
        }

        private function createOrthographicProjection(viewWidth:Number, viewHeight:Number,
                near:Number, far:Number):Matrix3D {
            return new Matrix3D(Vector.<Number>
                    ([ 2/viewWidth, 0, 0, 0,
                     0, -2/viewHeight, 0, 0,
                     0, 0, 1/(far-near), -near/(far-near),
                     0, 0, 0, 1 ]));
        }


        protected function enterFrame(e:Event):void {
            CModule.serviceUIRequests();
            CModule.callI(CModule.getPublicSymbol("update_external"), new Vector.<int>());
        }

        public function get consoleText():String {
            var txt:String = null;
            if(tf != null){
                txt = tf.text;
            }
            return txt;
        }
    }
}

