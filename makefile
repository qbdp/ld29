#CCACHE = $(shell which -p ccache 2> /dev/null)
LIBS =
SCFLAGS = -Wall -std=c++11
LDFLAGS =
SCC = csc
CC = gcc
CC_FLAGS = -fno-strict-aliasing -fwrapv
OPTIMIZATION =
DEFINES = -DDEBUG
FEATURES = -feature standalone
EMSCRIPTEN_SDK = ../../Downloads/emsdk-1.12.0-portable-32bit/emscripten/1.13.0/
XBRIDGE_SDK = /crossbridge/sdk/
CHICKEN_CORE = ../chicken-core/
RM = rm -fr

SCM_SRC = $(wildcard src/*.scm)
CHICKEN_SRC = runtime.c build-version.c library.c eval.c expand.c modules.c chicken-syntax.c\
	      chicken-config.h buildtag.h chicken.h\
	      srfi-1.c srfi-4.c srfi-13.c srfi-14.c srfi-69.c lolevel.c extras.c\
	      irregex.c scheduler.c files.c data-structures.c
LOCAL_CHICKEN = $(addprefix build/chicken/, $(CHICKEN_SRC))
LOCAL_CHICKEN_OBJ = $(filter-out %.h, $(LOCAL_CHICKEN:.c=.o))
EGGS = build/eggs/matchable build/eggs/glfw3-egg build/eggs/opengl build/eggs/stb_image-egg\
       build/eggs/freetype-egg
STANDALONE_LIBS = -static -lwsock32 -lglfw3 -lopengl32 -lgdi32 -lfreetype

# platform dependent stuff
UNAME = $(shell uname -s)
ifeq ($(UNAME), Linux)
    LIBS += 
    DEFINES += -DPLATFORM_LINUX
else
    LDFLAGS +=
    LIBS += 
    DEFINES += -DPLATFORM_WINDOWS
endif

all: rockets

# standalone common
.PHONY: standalone
standalone: standalone-setup $(LOCAL_CHICKEN_OBJ) $(EGGS) build/main.c
	$(CC) $(CC_FLAGS) $(DEFINES) -Ibuild/chicken/ $(LOCAL_CHICKEN_OBJ)\
	    build/main.c $(STANDALONE_LIBS) -o build/rockets

standalone-setup:
	mkdir -p build
	mkdir -p build/chicken
	mkdir -p build/eggs

build/main.c: $(SCM_SRC)
	chicken src/main.scm $(FEATURES) -output-file build/main.c

# TODO: use hacked apply for desktop standalone
$(LOCAL_CHICKEN_OBJ): $(LOCAL_CHICKEN)
	$(CC) $(CC_FLAGS) $(DEFINES) -DHAVE_CHICKEN_CONFIG_H -DC_NO_HACKED_APPLY -c\
	    $(@:.o=.c) -o $@

$(LOCAL_CHICKEN): 
	cp $(CHICKEN_CORE)$(notdir $@) $@

build/eggs/matchable:
	cd build/eggs && chicken-install -r matchable

build/eggs/glfw3-egg:
	-cd build/eggs && git clone git@github.com:Metanoia/glfw3-egg

build/eggs/opengl:
	cd build/eggs && chicken-install -r opengl

build/eggs/stb_image-egg:
	-cd build/eggs && git clone git@github.com:Metanoia/stb_image-egg

build/eggs/freetype-egg:
	-cd build/eggs && git clone git@github.com:Metanoia/freetype-egg


# js target
emscripten: CC = $(EMSCRIPTEN_SDK)emcc
emscripten: DEFINES += -DC_NONUNIX -DECOS -s LEGACY_GL_EMULATION=1 -s GL_UNSAFE_OPTS=0
emscripten: FEATURES += -feature emscripten
emscripten: STANDALONE_LIBS =
emscripten: standalone-setup $(LOCAL_CHICKEN) emscripten-setup $(LOCAL_CHICKEN_OBJ) $(EGGS) build/main.c
emscripten:
	$(CC) $(CC_FLAGS) $(DEFINES) -Ibuild/chicken/ $(LOCAL_CHICKEN_OBJ)\
	    build/main.c $(STANDALONE_LIBS) -o build/rockets.html

emscripten-setup:
	@# fix runtime.c
	sed -i -e "s/#include <cyg\/kernel\/kapi.h>/\/* removed by sed *\//g"\
	    build/chicken/runtime.c

# desktop target

.PHONY: rockets
rockets:
	$(SCC) -o $@ src/main.scm $(LDFLAGS) $(LIBS) $(OPTIMIZATION)

# flash target

.PHONY: flash
flash: CC = PATH=$(XBRIDGE_SDK)/../cygwin/bin/:$$PATH; export CYGWIN=nodosfilewarning;\
	    $(XBRIDGE_SDK)/usr/bin/gcc
flash: FEATURES += -feature flash
flash: STANDALONE_LIBS =
flash: standalone-setup $(LOCAL_CHICKEN_OBJ) $(EGGS) crossbridge/console.abc xvfs build/main.c
	PATH=$(XBRIDGE_SDK)/../cygwin/bin/:$$PATH;\
	     export CYGWIN=nodosfilewarning;\
	     $(XBRIDGE_SDK)/usr/bin/gcc -emit-swf -swf-size=800x600\
	     -symbol-abc=crossbridge/console.abc\
	     -Ibuild/chicken/\
	     $(LOCAL_CHICKEN_OBJ) build/main.c crossbridge/xvfs.abc -o rockets.swf\
	     $(STANDALONE_LIBS)
	@# signal end of compilation
	@echo -e "\007" && echo -e "\007" && echo -e "\007"

crossbridge/console.abc: xvfs
	cp $(XBRIDGE_SDK)/usr/share/LSOBackingStore.as crossbridge/
	java -jar $(XBRIDGE_SDK)/usr/lib/asc2.jar -md -merge -AS3 -strict -optimize \
	    -import $(XBRIDGE_SDK)/usr/lib/builtin.abc \
	    -import $(XBRIDGE_SDK)/usr/lib/playerglobal.abc \
	    -import $(XBRIDGE_SDK)/usr/lib/BinaryData.abc \
	    -import $(XBRIDGE_SDK)/usr/lib/ISpecialFile.abc \
	    -import $(XBRIDGE_SDK)/usr/lib/IBackingStore.abc \
	    -import $(XBRIDGE_SDK)/usr/lib/IVFS.abc \
	    -import $(XBRIDGE_SDK)/usr/lib/InMemoryBackingStore.abc \
	    -import $(XBRIDGE_SDK)/usr/lib/PlayerKernel.abc \
	    -import $(XBRIDGE_SDK)/usr/lib/CModule.abc\
	    -import crossbridge/xvfs.abc\
	    crossbridge/LSOBackingStore.as misc/AGALMiniAssembler.as misc/Console.as\
	    -outdir crossbridge -out console

# TODO: test compressed vfs
.PHONY:
xvfs:
	mkdir -p crossbridge/data
	cp -r data crossbridge/data
	cd crossbridge && PATH=$(XBRIDGE_SDK)/../cygwin/bin/:$$PATH;\
	    CYGWIN=nodosfilewarning;\
	    $(XBRIDGE_SDK)/usr/bin/genfs --type=embed --name=xvfs data/ xvfs
	$(RM) crossbridge/data
	java -jar $(XBRIDGE_SDK)/usr/lib/asc2.jar -merge -md -AS3 -strict -optimize \
	  -import $(XBRIDGE_SDK)/usr/lib/builtin.abc \
	  -import $(XBRIDGE_SDK)/usr/lib/playerglobal.abc \
	  -import $(XBRIDGE_SDK)/usr/lib/BinaryData.abc \
	  -import $(XBRIDGE_SDK)/usr/lib/ISpecialFile.abc \
	  -import $(XBRIDGE_SDK)/usr/lib/IBackingStore.aBC \
	  -import $(XBRIDGE_SDK)/usr/lib/IVFS.abc \
	  -import $(XBRIDGE_SDK)/usr/lib/InMemoryBackingStore.abc \
	  -import $(XBRIDGE_SDK)/usr/lib/PlayerKernel.abc \
	  crossbridge/xvfs_*.as -outdir crossbridge -out xvfs
		

# TODO: profile guided optimization
.PHONY:
#release: export OPTIMIZATION = -Ofast -mtune=generic -msse -mfpmath=sse -flto
release: export OPTIMIZATION = -Ofast -mtune=generic -msse -mfpmath=sse
release: export DEFINES = -DNDEBUG
#release: export LDFLAGS += -fwhole-program
release: all

.PHONY:
pack: release
	strip rockets*
	mkdir pack
	mkdir pack/rockets
	@#cp -R data pack/rockets
	pushd data/ && zip -r9 ../pack/rockets/main.dat * && popd
	-cp rockets pack/rockets
	-cp rockets.exe pack/rockets
	cd pack && zip -r9 ../rockets.zip rockets
	rm -fr pack

.PHONY:
publish: pack
	@echo "Key name for warsaw (default 'ssh_rsa'):"; \
	read key_name; \
	key_name=$${key_name:-ssh_rsa}; \
	scp -i ~/.ssh/$$key_name rockets.zip root@warsaw:/root/rockets.zip

.PHONY:
stats:
	@echo "Line count:" `cat src/*.cpp src/*.h | wc -l`
	@echo "Data folder size:" `du -c data -h | awk END{print} | sed s/\t.*$$//`
	@echo "Git commit count:" `git rev-list --count HEAD`

.PHONY:
run: rockets
	./rockets || true

.PHONY:
debug: all
	gdb rockets || true

.PHONY:
profile: LIBS += -pg
profile: release
	./rockets && gprof rockets.exe gmon.out > profile.txt

.PHONY:
clean:
	$(RM) build
	$(RM) rockets
	$(RM) rockets.exe
	$(RM) rockets.zip
	$(RM) crossbridge
	$(RM) rockets.swf
